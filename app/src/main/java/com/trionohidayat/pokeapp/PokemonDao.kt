package com.trionohidayat.pokeapp

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemon(pokemon: Pokemon)

    @Query("SELECT * FROM pokemon_table")
    suspend fun getAllPokemon(): List<Pokemon>

    @Query("SELECT * FROM pokemon_table WHERE name LIKE '%' || :name || '%'")
    suspend fun getPokemonByName(name: String): List<Pokemon>
}

