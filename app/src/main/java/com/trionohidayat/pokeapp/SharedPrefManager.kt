package com.trionohidayat.pokeapp

import android.content.Context
import android.content.SharedPreferences

object SharedPrefManager {

    private const val PREF_NAME = "MyPrefs"
    private const val WRITE_EXTERNAL_STORAGE_KEY = "write_external_storage_permission"

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    fun isWriteExternalStoragePermissionGranted(context: Context): Boolean {
        val sharedPrefs = getSharedPreferences(context)
        return sharedPrefs.getBoolean(WRITE_EXTERNAL_STORAGE_KEY, false)
    }

    fun setWriteExternalStoragePermission(context: Context, granted: Boolean) {
        val sharedPrefs = getSharedPreferences(context)
        with(sharedPrefs.edit()) {
            putBoolean(WRITE_EXTERNAL_STORAGE_KEY, granted)
            apply()
        }
    }
}
