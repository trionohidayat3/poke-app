package com.trionohidayat.pokeapp

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.trionohidayat.pokeapp.databinding.ActivityDetailBinding
import com.trionohidayat.pokeapp.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_POKEMON_DETAIL = "extra_pokemon_detail"
    }

    private lateinit var binding: ActivityDetailBinding
    private lateinit var layoutProgress: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        layoutProgress = binding.layoutProgress

        setSupportActionBar(binding.toolBar)
        binding.toolBar.setNavigationOnClickListener {
            onBackPressed()
        }

        val pokemonUrl = intent.getStringExtra(EXTRA_POKEMON_DETAIL)
        if (pokemonUrl != null) {
            fetchData(pokemonUrl)
        }

    }

    private fun fetchData(pokemonUrl: String) {
        layoutProgress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = ApiClient.apiService.getPokemonDetail(pokemonUrl)
                withContext(Dispatchers.Main) {
                    displayPokemonDetail(response)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                runOnUiThread {
                    layoutProgress.visibility = View.GONE
                }
            }
        }
    }

    private fun displayPokemonDetail(pokemonDetail: PokemonDetail) {
        binding.toolBar.title = pokemonDetail.name

        Glide.with(this)
            .load(pokemonDetail.sprites.other.home.front_default)
            .centerInside()
            .into(binding.imagePokemon)

        binding.tvAbilities.text = pokemonDetail.abilities.joinToString { it.ability.name }
        binding.tvBaseExperience.text = pokemonDetail.base_experience.toString()
    }
}