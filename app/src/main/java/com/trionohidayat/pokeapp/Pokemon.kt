package com.trionohidayat.pokeapp

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon_table")
data class Pokemon(
    val name: String,
    @PrimaryKey val url: String
)