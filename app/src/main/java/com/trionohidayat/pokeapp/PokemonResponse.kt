package com.trionohidayat.pokeapp

data class PokemonResponse(
    val results: List<Pokemon>
)