package com.trionohidayat.pokeapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences.Editor
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.search.SearchBar
import com.google.android.material.search.SearchView
import com.trionohidayat.pokeapp.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var searchBar: SearchBar
    private lateinit var textModeMain: TextView
    private lateinit var textNoInternet: TextView
    private lateinit var layoutProgress: LinearLayout
    private lateinit var recyclerMain: RecyclerView
    private lateinit var searchView: SearchView
    private lateinit var textModeSearch: TextView
    private lateinit var chipGroup: ChipGroup
    private lateinit var chipAsc: Chip
    private lateinit var chipDesc: Chip
    private lateinit var recyclerSearch: RecyclerView
    private lateinit var mainAdapter: MainAdapter
    private lateinit var searchAdapter: SearchAdapter
    private val pokemonListMain: MutableList<Pokemon> = mutableListOf()
    private val pokemonListSearch: MutableList<Pokemon> = mutableListOf()

    private var offset = 0
    private val limit = 20

    private lateinit var db: PokemonDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initialize()
        checkNetworkAvailability()
        if (isInternetAvailable(this)) {
            fetchData()
        } else {
            loadFromDatabase()
        }
    }

    private fun initialize() {
        initializeViews()
        setupRecyclerView()
        setupListeners()
        initializeDatabase()
    }

    private fun initializeViews() {
        searchBar = binding.searchBar
        textModeMain = binding.textModeMain
        textNoInternet = binding.textNoInternet
        layoutProgress = binding.layoutProgress
        recyclerMain = binding.recyclerMain
        searchView = binding.searchView
        textModeSearch = binding.textModeSearch
        chipGroup = binding.chipGroup
        chipAsc = binding.chipAsc
        chipDesc = binding.chipDesc
        recyclerSearch = binding.recyclerSearch
    }

    private fun setupRecyclerView() {
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

        recyclerMain.layoutManager = LinearLayoutManager(this)
        recyclerMain.addItemDecoration(itemDecoration)
        mainAdapter = MainAdapter(pokemonListMain)
        recyclerMain.adapter = mainAdapter

        recyclerSearch.layoutManager = LinearLayoutManager(this)
        recyclerSearch.addItemDecoration(itemDecoration)
        searchAdapter = SearchAdapter(pokemonListSearch)
        recyclerSearch.adapter = searchAdapter
    }

    private fun setupListeners() {
        searchBar.setOnClickListener {
            binding.searchView.show()
        }

        searchView.editText.setOnEditorActionListener { v, actionId, event ->
            performSearch(searchView.editText.text.toString())
            false
        }

        recyclerMain.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                ) {
                    fetchData()
                }
            }
        })

        chipGroup.setOnCheckedChangeListener { _, checkedId ->
            if (checkedId == R.id.chipAsc) {
                sortPokemonListByAscending()
            } else if (checkedId == R.id.chipDesc) {
                sortPokemonListByDescending()
            }
        }
    }

    private fun sortPokemonListByAscending() {
        pokemonListSearch.sortByDescending { it.name }
        pokemonListSearch.reverse()
        searchAdapter.notifyDataSetChanged()
    }

    private fun sortPokemonListByDescending() {
        pokemonListSearch.sortByDescending { it.name }
        searchAdapter.notifyDataSetChanged()
    }

    private fun initializeDatabase() {
        db = PokemonDatabase.getDatabase(this)
    }

    private fun performSearch(query: String) {
        layoutProgress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val pokemonListByName = db.pokemonDao().getPokemonByName(query)
                withContext(Dispatchers.Main) {
                    searchAdapter.updatePokemonList(pokemonListByName)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                runOnUiThread {
                    layoutProgress.visibility = View.GONE
                }
            }
        }
    }

    private fun checkNetworkAvailability() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                fetchData()
                updateUIOnlineMode()
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                loadFromDatabase()
                recyclerSearch.visibility = View.VISIBLE
            }
        }

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
    }

    private fun updateUIOnlineMode() {
        runOnUiThread {
            textModeMain.setModeText("Online Mode", android.R.color.holo_green_dark)
            textModeSearch.setModeText("Online Mode", android.R.color.holo_green_dark)
            chipGroup.visibility = View.GONE
            recyclerSearch.visibility = View.GONE
        }
    }

    private fun fetchData() {
        layoutProgress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = ApiClient.apiService.getPokemonList(offset, limit)
                withContext(Dispatchers.Main) {
                    if (response.results.isNotEmpty()) {
                        mainAdapter.addPokemon(response.results)
                        offset += limit

                        response.results.forEach { pokemon ->
                            val pokemonEntity = Pokemon(pokemon.name, pokemon.url)
                            GlobalScope.launch(Dispatchers.IO) {
                                db.pokemonDao().insertPokemon(pokemonEntity)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                runOnUiThread {
                    layoutProgress.visibility = View.GONE
                }
            }
        }
    }

    private fun loadFromDatabase() {
        layoutProgress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val pokemonListFromDb = db.pokemonDao().getAllPokemon()

                withContext(Dispatchers.Main) {
                    if (pokemonListFromDb.isNotEmpty()) {
                        textModeMain.setModeText("Offline Mode", android.R.color.holo_red_dark)
                        textModeSearch.setModeText("Offline Mode", android.R.color.holo_red_dark)
                        chipGroup.visibility = View.VISIBLE
                        textNoInternet.visibility = View.GONE
                        recyclerMain.visibility = View.VISIBLE
                        mainAdapter.updatePokemonList(pokemonListFromDb)
                        searchAdapter.updatePokemonList(pokemonListFromDb)
                    } else {
                        textNoInternet.visibility = View.VISIBLE
                        recyclerMain.visibility = View.GONE
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                runOnUiThread {
                    layoutProgress.visibility = View.GONE
                }
            }
        }
    }

    private fun TextView.setModeText(text: String, @DrawableRes background: Int) {
        this.text = text
        this.background = getDrawable(background)
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network)
        return capabilities != null &&
                (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
    }
}
