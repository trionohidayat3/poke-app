package com.trionohidayat.pokeapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.trionohidayat.pokeapp.databinding.ActivitySplashBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private val splashTimeOut: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        CoroutineScope(Dispatchers.Main).launch {
            delay(splashTimeOut)
            startMainActivity()
        }
    }

    private fun startMainActivity() {
        val intent = if (SharedPrefManager.isWriteExternalStoragePermissionGranted(this)) {
            Intent(this, MainActivity::class.java)
        } else {
            Intent(this, PermissionActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}